const EventEmitter = require('events');
const Promise = require("bluebird");
const _ = require('lodash');
const {
    v4: uuidv4
} = require('uuid');
const FS = require('fs').promises;
const NetdataAPI = require('./netdata.js');
const PingMetric = require('./metrics/ping.js');
const SnmpMetric = require('./metrics/snmp.js');

class Node extends EventEmitter {

    /**
     * Node
     *
     * @hideconstructor
     * @return {Node}  A Node.
     */
    constructor(configuration, api) {
        super();

        this._running = false;
        this._stopping = false;
        this._starting = false;

        this.configuration = _.defaults(configuration, {
            name: 'Virtual-Node',
            hostname: 'localhost',
            uuid: uuidv4(),
            update_every: 1,
            os: 'Generic',
            timezone: 'CEST'
        });

        this.api = api;
        this.sender = null;
        this.timer = null;
        this.metrics = [];
        this.charts = [];
        this.datapoints = [];
    }

    /**
     * Node started event.
     * Fired when the Node is fully started.
     *
     * @event Node#started
     */

    /**
     * Node stopped event.
     * Fired when the Nodes wants to stop.
     *
     * @event Node#stopped
     */


    /**
     * Start the Node.
     *
     * @fires Node#started
     * @return {Promise} Promise object, resolves if action is successfull, rejects otherwise.
     */
    start() {
        return new Promise((resolve, reject) => {
            if (this._running || this._starting)
                return resolve();

            this._starting = true;

            console.log('Node %s starting...', this.configuration.name);

            var actions = [];
            actions.push(() => this.createMetrics());
            actions.push(() => this.startMetrics());
            actions.push(() => this.startSender());
            Promise.reduce(actions, (undefined, fn) => fn(), null)
                .then(() => {
                    this._running = true;
                    this._starting = false;
                    this.emit('started');
                    console.log('Node %s started.', this.configuration.name);
                    resolve();
                })
                .catch((error) => {
                    this._running = false;
                    this._starting = false;
                    console.log('Node %s failed to start.', this.configuration.name);
                    console.dir(error);
                    reject(error);
                });
        });
    }

    /**
     * Stop the Node.
     *
     * @fires Node#stopped
     * @return {Promise} Promise object, resolves if action is successfull, rejects otherwise.
     */
    stop() {
        return new Promise((resolve, reject) => {
            if (this._stopping)
                return resolve();

            this._stopping = true;
            console.log('Node %s stopping...', this.configuration.name);

            var actions = [];
            actions.push(() => this.stopMetrics());
            actions.push(() => this.stopSender());
            Promise.reduce(actions, (undefined, fn) => fn().catch(e => e), null)
                .then(() => {
                    this._running = false;
                    this._stopping = false;
                    this.emit('stopped');
                    console.log('Node %s stopped.', this.configuration.name);
                    resolve();
                })
                .catch((error) => {
                    this._stopping = false;
                    console.log('Node %s failed to stop.', this.configuration.name);
                    reject(error);
                });
        });
    }

    /**
     * Whether the Node is running or not.
     *
     * @return {boolean}
     */
    isRunning() {
        return this._running;
    }

    /**
     * Whether the Node is starting or not.
     *
     * @return {boolean}
     */
    isStarting() {
        return this._starting;
    }

    /**
     * Whether the Node is stopping or not.
     *
     * @return {boolean}
     */
    isStopping() {
        return this._stopping;
    }

    /**
     * Start sending of data. Prepeare Netdata Node.
     * 1. Step:
     *     Create netdata node
     * 2. Step:
     *     Send charts to netdata server
     * 3. Step:
     *     Start timer to periodically send metrics.
     *
     * @return {Promise} Promise object, resolves if action is successfull, rejects otherwise.
     */
    startSender() {
        this.sender = this.api.createNode(this.configuration);
        return this.sender.start()
            .then(() => _.each(this.charts, (chart) => {
                this.sender.sendChart(chart);
            }))
            .then(() => {
                this.timer = setInterval(() => this.send(), 1000 * this.configuration.update_every);
            });
    }

    /**
     * Send datapoint to netdata server and clear buffer.
     *
     * @return {Promise} Promise object, resolves if action is successfull, rejects otherwise.
     */
    send() {
        return new Promise((resolve, reject) => {
            _.each(this.datapoints, (datapoint) => {
                var now = new Date().getTime();
                if (datapoint.microseconds != null) {
                    datapoint.time_elapsed = now - datapoint.microseconds;
                }
                datapoint.microseconds = now;
                this.sender.sendDatapoint(datapoint);
                _.map(datapoint.dimensions, (dimension) => {
                    return {
                        id: dimension.id,
                        value: null
                    }
                });
            });
            resolve();
        });
    }

    /**
     * Stop sending of data.
     *
     * @return {Promise} Promise object, resolves if action is successfull, rejects otherwise.
     */
    stopSender() {
        if (this.timer != null) {
            clearTimeout(this.timer);
        }
        return this.sender.stop();
    }

    /**
     * Create metrics from config.json.
     *
     * @return {Promise} Promise object, resolves if action is successfull, rejects otherwise.
     */
    createMetrics() {
        return new Promise((resolve, reject) => {
            _.each(this.configuration.metrics, (config) => {

                var metric_config = {
                    node: {
                        name: this.configuration.hostname,
                        hostname: this.configuration.hostname
                    },
                    update_every: this.configuration.update_every,
                }
                _.merge(metric_config, config);

                switch (config.name) {
                    case 'ping':
                        this.metrics.push(new PingMetric(metric_config));
                        break;
                    case 'snmp':
                        this.metrics.push(new SnmpMetric(metric_config));
                        break;

                    default:
                        break;
                }
            });
            resolve();
        });
    }

    /**
     * Start all metrics of the node.
     *
     * @return {Promise} Promise object, resolves if action is successfull, rejects otherwise.
     */
    startMetrics() {
        var actions = [];
        _.each(this.metrics, (metric) => {

            metric.on('addChart', (chart) => this.addChart(chart));
            metric.on('addDatapoint', (datapoint) => this.addDatapoint(datapoint));

            actions.push(() => metric.start());
        });
        return Promise.reduce(actions, (undefined, fn) => fn().catch(e => e), null);
    }

    /**
     * Stop all metrics of the node.
     *
     * @return {Promise} Promise object, resolves if action is successfull, rejects otherwise.
     */
    stopMetrics() {
        var actions = [];
        _.each(this.metrics, (metric) => {
            actions.push(() => metric.stop());
        });
        return Promise.reduce(actions, (undefined, fn) => fn().catch(e => e), null);
    }

    /**
     * Adds a chart to the node.
     *
     * @param {object} chart An object describing the chart
     * @return {Promise} Promise object, resolves if action is successfull, rejects otherwise.
     */
    addChart(chart) {
        return new Promise((resolve, reject) => {
            this.charts.push(chart);
            var datapoint = {
                id: chart.type + '.' + chart.id,
                microseconds: null,
                time_elapsed: null,
                dimensions: []
            }
            _.each(chart.dimensions, (dimension) => {
                datapoint.dimensions.push({
                    id: dimension.id,
                    value: null
                });
            });
            resolve(this.addDatapoint(datapoint));
        });
    }

    /**
     * Adds a datapoint to the node.
     *
     * Be sure to have the corresponding chart added first.
     *
     * @param {object} datapoint An object describing the datapoint
     * @return {Promise} Promise object, resolves if action is successfull, rejects otherwise.
     */
    addDatapoint(datapoint) {
        return new Promise((resolve, reject) => {
            var idx = _.findIndex(this.datapoints, {
                id: datapoint.id
            });
            if (idx > -1) {
                this.datapoints[idx].dimensions = datapoint.dimensions;
            } else {
                this.datapoints.push(datapoint);
            }
            resolve();
        });
    }
};

module.exports = Node;
