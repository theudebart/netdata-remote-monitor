const EventEmitter = require('events');
const Promise = require("bluebird");
const _ = require('lodash');
const Util = require('util');
const {
    v4: uuidv4
} = require('uuid');
const Net = require('net');

class NetdataAPI {

    constructor(definition) {
        _.defaults(definition, {
            server: {
                hostname: 'localhost',
                port: 19999
            },
            agent_name: 'netdata-remote-monitor/v0.0.1',
            agent_version: '2',
            protocol_version: '1.1',
            api_key: uuidv4()
        });
        _.merge(this, definition);
    }

    createNode(definition) {
        return new NetdataNode(definition, this);
    }

};

class NetdataNode extends EventEmitter {

    constructor(definition, api) {
        super();

        _.defaults(definition, {
            name: 'Virtual-Node',
            hostname: 'localhost',
            uuid: uuidv4(),
            update_every: 1,
            os: 'Generic',
            timezone: 'CEST',
            api: api
        });
        _.merge(this, definition);

        this.socket = new Net.Socket();
        this.socket.on('ready', () => {
            var buffer = Util.format('STREAM key=%s&hostname=%s&machine_guid=%s&update_every=%d&os=%s&timezone=%s&tags=&ver=%s HTTP/1.1\r\nUser-Agent: %s\r\nAccept: */*\r\n\r\n',
                this.api.api_key,
                this.name,
                this.uuid,
                this.update_every,
                this.os,
                this.timezone,
                this.api.agent_version,
                this.api.agent_name
            );
            this.socket.write(buffer);
            //console.log(buffer);
            console.log('New Netdata Node %s started', this.hostname);
        });

        this.socket.on('error', (error) => {
            console.log('Netdata Node %s %s', this.name, error);
            if (this.socket != null && !this.socket.destroyed)
                this.socket.destroy();
        });

        this.socket.on('data', (data) => {
            console.log('Netdata Node %s received: %s', this.name, data);
        });

        this.socket.on('timeout', () => {
            console.log('Netdata Node %s timeout', this.name);
        });

        this.socket.on('end', () => {
            console.log('Netdata Server wants to end the Connection with Netdata Node %s', this.name);
        });

        this.socket.on('close', () => {
            console.log('Netdata Node %s closed', this.name);
        });
    }

    /**
     * Netdata Node started event.
     * Fired when the Node is fully started.
     *
     * @event NetdataNode#started
     */

    /**
     * Netdata Node stopped event.
     * Fired when the Nodes wants to stop.
     *
     * @event NetdataNode#stopped
     */


    /**
     * Start the Netdata Node.
     *
     * @fires NetdataNode#started
     * @return {Promise} Promise object, resolves if action is successfull, rejects otherwise.
     */
    start() {
        return new Promise((resolve, reject) => {
            if (this._running || this._starting)
                return resolve();

            this._starting = true;

            console.log('Netdata Node %s starting...', this.hostname);

            var actions = [];
            actions.push(() => this.startConnection());
            Promise.reduce(actions, (undefined, fn) => fn(), null)
                .then(() => {
                    this._running = true;
                    this._starting = false;
                    this.emit('started');
                    console.log('Netdata Node %s started.', this.hostname);
                    resolve();
                })
                .catch((error) => {
                    this._running = false;
                    this._starting = false;
                    console.log('Netdata Node %s failed to start.', this.hostname);
                    console.dir(error);
                    reject(error);
                });
        });
    }

    /**
     * Stop the Node.
     *
     * @fires Node#stopped
     * @return {Promise} Promise object, resolves if action is successfull, rejects otherwise.
     */
    stop() {
        return new Promise((resolve, reject) => {
            if (this._stopping)
                return resolve();

            this._stopping = true;
            console.log('Netdata Node %s stopping...', this.hostname);

            var actions = [];
            actions.push(() => this.stopConnection());
            Promise.reduce(actions, (undefined, fn) => fn().catch(e => e), null)
                .then(() => {
                    this._running = false;
                    this._stopping = false;
                    this.emit('stopped');
                    console.log('Netdata Node %s stopped.', this.hostname);
                    resolve();
                })
                .catch((error) => {
                    this._stopping = false;
                    console.log('Netdata Node %s failed to stop.', this.hostname);
                    reject(error);
                });
        });
    }

    /**
     * Whether the Node is running or not.
     *
     * @return {boolean}
     */
    isRunning() {
        return this._running;
    }

    /**
     * Whether the Node is starting or not.
     *
     * @return {boolean}
     */
    isStarting() {
        return this._starting;
    }

    /**
     * Whether the Node is stopping or not.
     *
     * @return {boolean}
     */
    isStopping() {
        return this._stopping;
    }

    /**
     * Start connection.
     *
     * @return {Promise} Promise object, resolves if action is successfull, rejects otherwise.
     */
    startConnection() {
        return new Promise((resolve, reject) => {
            this.reconnect_timer = setInterval(() => this.reconnectConnection(), 3000);
            this.socket.connect(this.api.server.port, this.api.server.hostname, () => {
                resolve();
            });
        });
    }

    /**
     * Reconnect connection if connection is broken.
     *
     * @return {Promise} Promise object, resolves if action is successfull, rejects otherwise.
     */
    reconnectConnection() {
        if (this.socket.destroyed) {
            console.log('Netdata Node %s reconnect...', this.name)
            return this.startConnection();
        } else {
            return Promise.resolve();
        }
    }

    /**
     * Stop connection.
     *
     * @return {Promise} Promise object, resolves if action is successfull, rejects otherwise.
     */
    stopConnection() {
        return new Promise((resolve, reject) => {
            if (this.reconnect_timer != null) {
                clearTimeout(this.reconnect_timer);
                this.reconnect_timer = null;
            }

            if (!this.socket.destroyed) {
                resolve(this.socket.end())
            } else {
                resolve();
            }
        });
    }

    /**
     * Sends a chart command to the netdata server.
     *
     * @param {object} chart An object describing the chart
     * @return {Promise} Promise object, resolves if action is successfull, rejects otherwise.
     */
    sendChart(chart) {
        return new Promise((resolve, reject) => {

            if (this.socket.destroyed) {
                resolve();
                return;
            }

            chart = _.defaults(chart, {
                type: '',
                id: '',
                name: null,
                title: '',
                units: '',
                family: '',
                context: '',
                charttype: 'line',
                priority: 1000,
            });
            var buffer = Util.format('CHART "%s.%s" "%s" "%s" "%s" "%s" "%s" "%s" "%s"\r\n',
                chart.type,
                chart.id,
                chart.name,
                chart.title,
                chart.units,
                chart.family,
                chart.context,
                chart.charttype,
                chart.priority
            );
            _.each(chart.dimensions, (dimension) => {
                dimension = _.defaults(dimension, {
                    id: '',
                    name: '',
                    algorithm: 'absolute',
                    multiplier: 1,
                    divisor: 1,
                });
                buffer += Util.format('DIMENSION "%s" "%s" "%s" %d %d\r\n',
                    dimension.id,
                    dimension.name,
                    dimension.algorithm,
                    dimension.multiplier,
                    dimension.divisor,
                );
            });
            buffer += '\r\n';
            //console.log(buffer);
            resolve(this.socket.write(buffer));
        });
    }

    /**
     * Sends a datapoint command to the netdata server.
     *
     * @param {object} datapoint An object describing the datapoint
     * @return {Promise} Promise object, resolves if action is successfull, rejects otherwise.
     */
    sendDatapoint(datapoint) {
        return new Promise((resolve, reject) => {

            if (this.socket.destroyed) {
                resolve();
                return;
            }

            datapoint = _.defaults(datapoint, {
                id: '',
                time_elapsed: null,
            });
            //Argument microsecnonds seams not supported?
            /*
            if (datapoint.time_elapsed != null) {
                var buffer = Util.format('BEGIN "%s" %d\r\n',
                    datapoint.id,
                    datapoint.time_elapsed,
                );
            } else {
            */
            var buffer = Util.format('BEGIN "%s"\r\n',
                datapoint.id
            );
            /*
            }
            */
            _.each(datapoint.dimensions, (dimension) => {
                if (dimension.value != null) {
                    buffer += Util.format('SET "%s" = %d\r\n',
                        dimension.id,
                        dimension.value
                    );
                } else {
                    buffer += Util.format('SET "%s" =\r\n',
                        dimension.id
                    );
                }
            });
            buffer += 'END\r\n\r\n';
            //console.log(buffer);
            resolve(this.socket.write(buffer));
        });
    }

};

module.exports = NetdataAPI;
