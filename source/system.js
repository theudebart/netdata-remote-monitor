const EventEmitter = require('events');
const Promise = require("bluebird");
const _ = require('lodash');
const {
    v4: uuidv4
} = require('uuid');
const FS = require('fs').promises;
const NetdataAPI = require('./netdata.js');
const Node = require('./node.js');

class System extends EventEmitter {

    /**
     * System
     *
     * @hideconstructor
     * @return {System}  A System.
     */
    constructor() {
        super();

        this._running = false;
        this._stopping = false;
        this._starting = false;

        this.configuration = {};
        this.nodes = [];

        process.on('SIGINT', () => {
            if (this.isRunning() && !this.isStopping())
                this.stop().then(() => process.exit(0)).catch((error) => process.exit(1));
        });
        process.on('SIGTERM', () => {
            if (this.isRunning() && !this.isStopping())
                this.stop().then(() => process.exit(0)).catch((error) => process.exit(1));
        });
    }

    /**
     * System started event.
     * Fired when the system is fully started.
     *
     * @event System#started
     */

    /**
     * System stopped event.
     * Fired when the systems wants to stop.
     *
     * @event System#stopped
     */


    /**
     * Start the system.
     *
     * @fires System#started
     * @return {Promise} Promise object, resolves if action is successfull, rejects otherwise.
     */
    start() {
        return new Promise((resolve, reject) => {
            if (this._running || this._starting)
                return resolve();

            this._starting = true;

            console.log('System starting...');

            var actions = [];
            actions.push(() => this.readConfiguration());
            actions.push(() => this.createNodes());
            actions.push(() => this.startNodes());
            Promise.reduce(actions, (undefined, fn) => fn(), null)
                .then(() => {
                    this._running = true;
                    this._starting = false;
                    this.emit('started');
                    console.log('System started.');
                    resolve();
                })
                .catch((error) => {
                    this._running = false;
                    this._starting = false;
                    console.log('System failed to start.');
                    reject(error);
                });
        });
    }

    /**
     * Stop the system.
     *
     * @fires System#stopped
     * @return {Promise} Promise object, resolves if action is successfull, rejects otherwise.
     */
    stop() {
        return new Promise((resolve, reject) => {
            if (this._stopping)
                return resolve();

            this._stopping = true;
            console.log('System stopping...');

            var actions = [];
            actions.push(() => this.stopNodes());
            actions.push(() => this.saveConfiguration());
            Promise.reduce(actions, (undefined, fn) => fn().catch(e => e), null)
                .then(() => {
                    this._running = false;
                    this._stopping = false;
                    this.emit('stopped');
                    console.log('System stopped.');
                    resolve();
                })
                .catch((error) => {
                    this._stopping = false;
                    console.log('System failed to stop.');
                    reject(error);
                });
        });
    }

    /**
     * Whether the system is running or not.
     *
     * @return {boolean}
     */
    isRunning() {
        return this._running;
    }

    /**
     * Whether the system is starting or not.
     *
     * @return {boolean}
     */
    isStarting() {
        return this._starting;
    }

    /**
     * Whether the system is stopping or not.
     *
     * @return {boolean}
     */
    isStopping() {
        return this._stopping;
    }

    /**
     * Read configuration to config.json.
     *
     * @return {Promise} Promise object, resolves if action is successfull, rejects otherwise.
     */
    readConfiguration() {
        return FS.readFile('config.json')
            .then((data) => JSON.parse(data))
            .then((configuration) => {
                _.defaults(configuration, {
                    server: {
                        hostname: 'localhost',
                        port: 19999
                    },
                    api_key: uuidv4(),
                    nodes: []
                });
                _.merge(this.configuration, configuration);
            });
    }

    /**
     * Save configuration to config.json.
     *
     * @return {Promise} Promise object, resolves if action is successfull, rejects otherwise.
     */
    saveConfiguration() {
        return new Promise((resolve, reject) => {
                this.configuration.nodes = _.map(this.nodes, (node) => _.omit(node.configuration, 'api'));
                resolve(this.configuration);
            })
            .then((configuration) => JSON.stringify(configuration, null, '\t'))
            .then((data) => FS.writeFile('config.json', data));
    }

    /**
     * Create nodes from config.json.
     *
     * @return {Promise} Promise object, resolves if action is successfull, rejects otherwise.
     */
    createNodes() {
        return new Promise((resolve, reject) => {
            this.api = new NetdataAPI({
                server: {
                    hostname: this.configuration.server.hostname,
                    port: this.configuration.server.port
                },
                api_key: this.configuration.api_key
            });

            _.each(this.configuration.nodes, (config) => {
                this.nodes.push(new Node(config, this.api));
            });
            resolve();
        });
    }

    /**
     * Start all nodes of the system.
     *
     * @return {Promise} Promise object, resolves if action is successfull, rejects otherwise.
     */
    startNodes() {
        var actions = [];
        _.each(this.nodes, (node) => {
            actions.push(() => node.start());
        });
        return Promise.reduce(actions, (undefined, fn) => fn().catch(e => e), null);
    }

    /**
     * Stop all nodes of the system.
     *
     * @return {Promise} Promise object, resolves if action is successfull, rejects otherwise.
     */
    stopNodes() {
        var actions = [];
        _.each(this.nodes, (node) => {
            actions.push(() => node.stop());
        });
        return Promise.reduce(actions, (undefined, fn) => fn().catch(e => e), null);
    }

};

module.exports = System;
