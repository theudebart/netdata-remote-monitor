const System = require("./system.js");

console.log('');
console.log('');
console.log('');
console.log('  _   _ _____ _____ ____    _  _____  _                                         ');
console.log(' | \\ | | ____|_   _|  _ \\  / \\|_   _|/ \\                                        ');
console.log(' |  \\| |  _|   | | | | | |/ _ \\ | | / _ \\                                       ');
console.log(' | |\\  | |___  | | | |_| / ___ \\| |/ ___ \\                                      ');
console.log(' |_|_\\_|_____|_|_|_|____/_/___\\_\\_/_/   \\_\\ __  ___  _   _ ___ _____ ___  ____  ');
console.log(' |  _ \\| ____|  \\/  |/ _ \\_   _| ____| |  \\/  |/ _ \\| \\ | |_ _|_   _/ _ \\|  _ \\ ');
console.log(' | |_) |  _| | |\\/| | | | || | |  _|   | |\\/| | | | |  \\| || |  | || | | | |_) |');
console.log(' |  _ <| |___| |  | | |_| || | | |___  | |  | | |_| | |\\  || |  | || |_| |  _ < ');
console.log(' |_| \\_\\_____|_|  |_|\\___/ |_| |_____| |_|  |_|\\___/|_| \\_|___| |_| \\___/|_| \\_\\');
console.log('');
console.log('');
console.log('Netdata Remote Monitor V0.0.1');
console.log('');
console.log('');

let system = new System();
system.start()
.catch((error) => {
    console.log(error);
    system.stop();
});
