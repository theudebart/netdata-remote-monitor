const EventEmitter = require('events');
const Promise = require("bluebird");

class Metric extends EventEmitter {

    /**
     * Metric Prototype
     *
     * This is a protype of a metric. Every metric should inherit from this prototype.
     * Methods onSetup, onStart, onStop and onUpdateInterval should be overriden by own methods.
     *
     * @hideconstructor
     * @return {Metric}  A Metric Prototype.
     */
    constructor(configuration) {
        super();

        this._running = false;
        this._stopping = false;
        this._starting = false;

        this.configuration = configuration;

        this.update_interval = null;
    }

    /**
     * Metric started event.
     * Fired when the Metric is fully started.
     *
     * @event Metric#started
     */

    /**
     * Metric stopped event.
     * Fired when the Metrics wants to stop.
     *
     * @event Metric#stopped
     */


    /**
     * Start the Metric.
     *
     * @fires Metric#started
     * @return {Promise} Promise object, resolves if action is successfull, rejects otherwise.
     */
    start() {
        return new Promise((resolve, reject) => {
            if (this._running || this._starting)
                return resolve();

            this._starting = true;

            console.log('Metric %s of Node %s starting...', this.configuration.name, this.configuration.node.name);

            var actions = [];
            actions.push(() => this.onSetup());
            actions.push(() => this.onStart());
            actions.push(() => this.startUpdateInterval());
            Promise.reduce(actions, (undefined, fn) => fn(), null)
                .then(() => {
                    this._running = true;
                    this._starting = false;
                    this.emit('started');
                    console.log('Metric %s of Node %s started.', this.configuration.name, this.configuration.node.name);
                    resolve();
                })
                .catch((error) => {
                    this._running = false;
                    this._starting = false;
                    console.log('Metric %s of Node %s failed to start.', this.configuration.name, this.configuration.node.name);
                    console.dir(error);
                    reject(error);
                });
        });
    }

    /**
     * Stop the Metric.
     *
     * @fires Metric#stopped
     * @return {Promise} Promise object, resolves if action is successfull, rejects otherwise.
     */
    stop() {
        return new Promise((resolve, reject) => {
            if (this._stopping)
                return resolve();

            this._stopping = true;
            console.log('Metric %s of Node %s stopping...', this.configuration.name, this.configuration.node.name);

            var actions = [];
            actions.push(() => this.stopUpdateInterval());
            actions.push(() => this.onStop());
            Promise.reduce(actions, (undefined, fn) => fn().catch(e => e), null)
                .then(() => {
                    this._running = false;
                    this._stopping = false;
                    this.emit('stopped');
                    console.log('Metric %s of Node %s stopped.', this.configuration.name, this.configuration.node.name);
                    resolve();
                })
                .catch((error) => {
                    this._stopping = false;
                    console.log('Metric %s of Node %s failed to stop.', this.configuration.name, this.configuration.node.name);
                    reject(error);
                });
        });
    }

    /**
     * Whether the Metric is running or not.
     *
     * @return {boolean}
     */
    isRunning() {
        return this._running;
    }

    /**
     * Whether the Metric is starting or not.
     *
     * @return {boolean}
     */
    isStarting() {
        return this._starting;
    }

    /**
     * Whether the N
     *
     * ode is stopping or not.
     *
     * @return {boolean}
     */
    isStopping() {
        return this._stopping;
    }

    /**
     * Adds a chart to the node.
     *
     * @param {object} chart An object describing the chart
     * @return {Promise} Promise object, resolves if action is successfull, rejects otherwise.
     */
    addChart(chart) {
        return new Promise((resolve, reject) => {
            this.emit('addChart', chart);
            resolve();
        });
    }

    /**
     * Adds a datapoint to the node.
     *
     * Be sure to have the corresponding chart added first.
     *
     * @param {object} datapoint An object describing the datapoint
     * @return {Promise} Promise object, resolves if action is successfull, rejects otherwise.
     */
    addDatapoint(datapoint) {
        return new Promise((resolve, reject) => {
            this.emit('addDatapoint', datapoint);
            resolve();
        });
    }

    /**
     * Start the update interval.
     *
     * @return {Promise} Promise object, resolves if action is successfull, rejects otherwise.
     */
    startUpdateInterval() {
        return new Promise((resolve, reject) => {
            this.update_interval = setInterval(() => this.onUpdateInterval(), 1000 * this.configuration.update_every);
            resolve();
        });
    }

    /**
     * Stop the update interval.
     *
     * @return {Promise} Promise object, resolves if action is successfull, rejects otherwise.
     */
    stopUpdateInterval() {
        return new Promise((resolve, reject) => {
            if (this.update_interval != null) {
                clearTimeout(this.update_interval);
                this.update_interval = null;
            }
            resolve();
        });
    }

    /**
     * Override this method and do initilizing of the metric.
     * Charts should be introduced here with this.addChart(chart).
     *
     * @return {Promise} Promise object, resolves if action is successfull, rejects otherwise.
     */
    onSetup() {
        return new Promise.resolve();
    }

    /**
     * Override this method and start the metric.
     *
     * @return {Promise} Promise object, resolves if action is successfull, rejects otherwise.
     */
    onStart() {
        return new Promise.resolve();
    }

    /**
     * Override this method if needed.
     * Datapoints should be published here with this.addDatapoint(datapoint).
     *
     * @return {Promise} Promise object, resolves if action is successfull, rejects otherwise.
     */
    onUpdateInterval() {
        return new Promise.resolve();
    }

    /**
     * Override this method and stop the metric.
     *
     * @return {Promise} Promise object, resolves if action is successfull, rejects otherwise.
     */
    onStop() {
        return new Promise.resolve();
    }
};

module.exports = Metric;
