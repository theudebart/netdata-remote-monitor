const Metric = require('./prototype.js');
const Promise = require('bluebird');
const _ = require('lodash');
const DNS = require('dns')
const Ping = require('net-ping');

class PingMetric extends Metric {

    /**
     * Ping Metric
     *
     * @hideconstructor
     * @return {PingMetric}  A Ping Metric.
     */
    constructor(configuration) {
        super(_.defaults(configuration, {
            ping_every: 200,
            charts: [
                'latency',
                'quality',
                'packets'
            ],
            ip: ''
        }));

        this.latency = {
            min: null,
            max: null,
            avg: null
        };
        this.packets = {
            sent: 0,
            received: 0,
            lost: 0
        };

        this.ping_session = null;
    }

    /**
     * Setup the Ping Metric
     * Introduce charts and configure ping session.
     *
     * @override
     * @return {Promise} Promise object, resolves if action is successfull, rejects otherwise.
     */
    onSetup() {
        return new Promise((resolve, reject) => {
            _.each(this.configuration.charts, (chart) => {
                switch (chart) {
                    case 'latency':
                        this.addChart({
                            type: 'ping',
                            id: 'latency',
                            title: 'Latency',
                            units: 'ms',
                            context: 'ping_latency',
                            charttype: 'area',
                            dimensions: [{
                                    id: 'min',
                                    name: 'minimum',
                                    algorithm: 'absolute',
                                    divisor: 1000
                                },
                                {
                                    id: 'max',
                                    name: 'maximum',
                                    algorithm: 'absolute',
                                    divisor: 1000
                                },
                                {
                                    id: 'avg',
                                    name: 'average',
                                    algorithm: 'absolute',
                                    divisor: 1000
                                }
                            ]
                        });
                        break;
                    case 'packets':
                        this.addChart({
                            type: 'ping',
                            id: 'packets',
                            title: 'Pakets',
                            units: 'packets',
                            context: 'ping_packets',
                            dimensions: [{
                                    id: 'sent',
                                    name: 'sent',
                                    algorithm: 'absolute'
                                },
                                {
                                    id: 'received',
                                    name: 'received',
                                    algorithm: 'absolute'
                                }
                            ]
                        });
                        break;
                    case 'quality':
                        this.addChart({
                            type: 'ping',
                            id: 'quality',
                            title: 'Quality',
                            units: 'percentage',
                            context: 'ping_quality',
                            dimensions: [{
                                id: 'returned',
                                name: 'returned',
                                algorithm: 'absolute'
                            }]
                        });
                        break;
                    default:
                        break;
                }
            });

            this.ping_session = Ping.createSession({
                networkProtocol: Ping.NetworkProtocol.IPv4,
                packetSize: 16,
                retries: 1,
                sessionId: (process.pid % 65535),
                timeout: 1000,
                ttl: 128
            });

            DNS.lookup(this.configuration.node.hostname, (error, result) => {
                if (!error) {
                    this.configuration.node.ip = result
                }
                resolve();
            });
        });
    }

    /**
     * Publish Datapoints
     * publish statistics and reset them.
     *
     * @override
     * @return {Promise} Promise object, resolves if action is successfull, rejects otherwise.
     */
    onUpdateInterval() {
        return new Promise((resolve, reject) => {

            if ((this.packets.received + this.packets.lost) > this.packets.sent) {
                this.packets.sent = (this.packets.received + this.packets.lost);
            }

            _.each(this.configuration.charts, (chart) => {
                switch (chart) {
                    case 'latency':
                        this.addDatapoint({
                            id: 'ping.latency',
                            dimensions: [{
                                    id: 'min',
                                    value: this.latency.min
                                },
                                {
                                    id: 'max',
                                    value: this.latency.max
                                },
                                {
                                    id: 'avg',
                                    value: this.latency.avg
                                }
                            ]
                        });
                        break;
                    case 'packets':
                        this.addDatapoint({
                            id: 'ping.packets',
                            dimensions: [{
                                    id: 'sent',
                                    value: this.packets.sent
                                },
                                {
                                    id: 'received',
                                    value: this.packets.received
                                }
                            ]
                        });
                        break;
                    case 'quality':
                        this.addDatapoint({
                            id: 'ping.quality',
                            dimensions: [{
                                id: 'returned',
                                value: Math.floor((this.packets.received / this.packets.sent) * 100)
                            }]
                        });
                        break;
                    default:
                        break;
                }
            });

            this.latency.min = null;
            this.latency.avg = null;
            this.latency.max = null;
            this.packets.sent = 0;
            this.packets.received = 0;
            this.packets.lost = 0;
            resolve();
        });
    }

    /**
     * Ping the node.
     * Ping the node and at the results to the statistic.
     *
     * @return {Promise} Promise object, resolves if action is successfull, rejects otherwise.
     */
    ping() {
        return new Promise((resolve, reject) => {
            this.ping_session.pingHost(this.configuration.node.ip, (error, target, sent, rcvd) => {
                var latency = (rcvd - sent) * 1000;
                if (error) {
                    this.packets.lost++;
                } else {
                    this.packets.received++;
                    if (this.latency.min == null || latency < this.latency.min)
                        this.latency.min = latency;
                    if (this.latency.max == null || latency > this.latency.max)
                        this.latency.max = latency;
                    if (this.latency.avg != null)
                        this.latency.avg = Math.floor((latency + this.latency.avg) / 2);
                    else
                        this.latency.avg = latency;
                }
            });
            this.packets.sent++;
            resolve();
        });
    }

    /**
     * Start the Ping Metric
     * Start the ping interval.
     *
     * @override
     * @return {Promise} Promise object, resolves if action is successfull, rejects otherwise.
     */
    onStart() {
        return new Promise((resolve, reject) => {
            this.interval = setInterval(() => this.ping(), this.configuration.ping_every);
            resolve();
        });
    }

    /**
     * Stop the Ping Metric
     * Stop the ping interval.
     *
     * @override
     * @return {Promise} Promise object, resolves if action is successfull, rejects otherwise.
     */
    onStop() {
        return new Promise((resolve, reject) => {
            if (this.interval != null) {
                clearTimeout(this.interval);
                this.interval = null;
            }
            resolve();
        });
    }
};

module.exports = PingMetric;
