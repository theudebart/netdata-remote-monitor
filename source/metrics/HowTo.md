### Metrics

It is easy to add new metrics. All metric types are defined in source/metrics. The Prototype is source/metrics/prototype.js.

Just override the following functions.

**onSetup**

Override this method and do initializing of the metric.
Charts should be introduced here with this.addChart(chart).

**onStart()**

Override this method and start the metric.

**onUpdateInterval()**

Override this method if needed.
Datapoints should be published here with this.addDatapoint(datapoint).

**onStop()**

Override this method and stop the metric.

From within the metric you can use the functions /this.addChart(chart)/ and /this.addDatapoint(datapoint)/. Just be sure to add a chart first and in the onSetup function before you utilize /this.addDatapoint(datapoint)/.
