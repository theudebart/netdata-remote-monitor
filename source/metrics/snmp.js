const Metric = require('./prototype.js');
const Promise = require('bluebird');
const _ = require('lodash');
const SNMP = require('snmp-native');

class SnmpMetric extends Metric {

    /**
     * SNMP Metric
     *
     * @hideconstructor
     * @return {SnmpMetric}  A SNMP Metric.
     */
    constructor(configuration) {
        super(_.defaults(configuration, {
            community: 'public',
            port: 161,
            timeout: 5000
        }));

        this.snmp_session = null;
    }

    /**
     * Setup the SNMP Metric
     * Introduce charts and configure SNMP session.
     *
     * @override
     * @return {Promise} Promise object, resolves if action is successfull, rejects otherwise.
     */
    onSetup() {
        return new Promise((resolve, reject) => {
            _.each(this.configuration.charts, (chart) => {
                this.addChart(chart);
            });

            this.snmp_session = new SNMP.Session({
                host: this.configuration.node.hostname,
                port: this.configuration.port,
                community: this.configuration.community
            });
            resolve();
        });
    }

    /**
     * Publish Datapoints
     * Collect all snmp data and publish them.
     *
     * @override
     * @return {Promise} Promise object, resolves if action is successfull, rejects otherwise.
     */
    onUpdateInterval() {
        return new Promise((resolve, reject) => {
            _.each(this.configuration.charts, (chart) => {
                Promise.map(chart.dimensions, (dimension) => new Promise((resolve, reject) => {
                        this.snmp_session.get({
                            oid: dimension.oid
                        }, (error, variables) => {
                            if (error) {
                                resolve({
                                    id: dimension.id,
                                    value: null
                                });
                            } else if (variables[0] != undefined) {
                                resolve({
                                    id: dimension.id,
                                    value: Math.floor(variables[0].value)
                                });
                            }
                        });
                    }))
                    .then((dimensions) => {
                        this.addDatapoint({
                            id: chart.type + '.' + chart.id,
                            dimensions: dimensions
                        });
                        resolve();
                    });
            });
            resolve();
        });
    }

    /**
     * Start the SNMP Metric
     * Nothing to do here.
     *
     * @override
     * @return {Promise} Promise object, resolves if action is successfull, rejects otherwise.
     */
    onStart() {
        return new Promise((resolve, reject) => {
            resolve();
        });
    }

    /**
     * Stop the SNMP Metric
     * Nothing to do here.
     *
     * @override
     * @return {Promise} Promise object, resolves if action is successfull, rejects otherwise.
     */
    onStop() {
        return new Promise((resolve, reject) => {
            resolve();
        });
    }
};

module.exports = SnmpMetric;
