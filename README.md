# Netdata Remote Monitor

This application monitors remote machines which have no own netdata installation and presents the collected metrics to a netdata master as own nodes.

## installation

1. Download and install NodeJS from https://nodejs.org/en/download/ if not already installed.
2. Download (Cline) this repository to your chosen destination `git clone https://gitlab.com/theudebart/netdata-remote-monitor.git`.
3. Install application with `npm install`.
4. Create config.json. See chapter configuration.
5. Run application with `npm start`.

## Configuration

All configuration is done trough the config.json. See the config_example.json for help.

### Net Data Server

The server to which the nodes shall publish needs at least the hostname and a api_key from the netdata master server.
```
server": {
    "hostname": "localhost",
    "port": "19999"
},
"agent_name": "netdata-remote-monitor/v0.0.1",
"api_key": "3b48fe3c-8bc4-4377-b431-cc5de61fb246"
```
If no api_key is given a new one will be made and stored in the config.json.
Additionally the agent name which will be reported to the netdata master server can be set.

### Nodes

Multiple nodes can be defined with the nodes array. Every node will be added as separate node on the netdata master server and so will appear in the top left menu.

At least the node name which will appear in the menu, the hostname to reach the node and a uuid has to be given. If no uuid is given a new one will be made and stored in the config.json. The other values defaults to the below.
```
"name": "Virtual-Node",
"hostname": "localhost",
"uuid": "",
"update_every": 1,
"os": "Generic",
"timezone": "CEST"
```
update_every is the update interval in seconds.
os can be chosen freely.

### Metrics

Every node can have different metrics. In the metrics array of an node the metrics are added and configured. The name decides which type of metric should be added. Every metric can have a different configuration.

The supported metrics are "ping" and "snmp".

**Ping**

The following configuration can be made to the ping metric and defaults to
```
"name": "ping",
"ping_every": 200,
"charts": [
    "latency",
    "quality",
    "packets"
]
```
ping_every is the ping interval in milliseconds.
charts define which type of charts shall be created. Available are "latency", "quality" and "packets".

**SNMP**

The following configuration can be made to the snmp metric
```
"community": "public",
"port": 161,
"timeout": 5000
```
ping_every is the ping interval in milliseconds.
charts define which type of charts shall be created. Available are "latency", "quality" and "packets".

Charts which uses oid values have then do be defined. See the example below.
```
"charts": [
    {
        "type": "net",
        "id": "port1",
        "title": "Bandwidth",
        "units": "kilobits/s",
        "family": "port1",
        "charttype": "area",
        "dimensions": [
            {
                "id": "received",
                "name": "received",
                "oid": ".1.3.6.1.2.1.2.2.1.10.7",
                "algorithm": "incremental",
                "multiplier": 8,
                "divisor": 1024
            }
        ]
    }
]
```

## Feature Request

To request a feature, open an issue on the [Issue Board](https://gitlab.com/theudebart/netdata-remote-monitor/-/issues). Please use the following template for a feature request:
```markdown
## Feature Request

### Summary:
Short summary of the requested feature.

### Description:
Detailed description of the requested feature.

/label ~"Feature Request"
```

## Support

For support the [Issue Board](https://gitlab.com/theudebart/netdata-remote-monitor/-/issues) can be used. Also bugs can be reported here, for a bug report please use the following template:
```markdown
## Bug Overview

### Summary:
Describe the bug here shortly.

## Enviroment

### Operation System:
Which operating system is you device running?

## Bug Details

### Steps to reproduce:
What have you done to get this bug?

### Expected result:
What did you expect would happen?

### Actual result:
What was the actual behaviour of the application?

### Description:
If needed, add a longer description of the bug here.

/label ~"Bug"
```

## Authors

Main Author of this project is [@theudebart](https://gitlab.com/theudebart).
Feel free to contribute!

## License

This project is licensed under GNU GPLv3, feel free to do what ever you want. I would appreciate it, if you would name me in a remixed project or just directly influence this project with merge requests.

## Contribute

Feel free to contribute to this project. Just use a merge request.
